using System;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using StreamService.Models;
using UsersService.Helpers;

namespace StreamService.Hubs
{
    [Authorize]
    public class ChatHub : Hub
    {
        private IUserProvider _userProvider;
        
        public ChatHub(IUserProvider userProvider)
        {
            this._userProvider = userProvider;
        }
        [Authorize]
        public async Task JoinClub(string chatName)
        {
            Console.WriteLine("456");
            await Groups.AddToGroupAsync(Context.ConnectionId, chatName);
        }

        [Authorize]
        public async Task SendMessageToClub(string clubName, string message)
        {
            Console.WriteLine("123");
         var chatMessage = new ChatMessage()
         {
             Id = Guid.NewGuid().ToString(),
             ClubName = clubName,
             UserName = _userProvider.GetUsername(),
             Date = DateTime.Now.ToString(),
             Message = message
         };   
         Console.WriteLine(chatMessage.Message);
          await Clients.Group(clubName).SendAsync("MessageReceived", chatMessage);
        }
        
        public async Task LeaveClub(string clubName)
        {
            var chatMessage = new ChatMessage()
            {
                Id = Guid.NewGuid().ToString(),
                ClubName = clubName,
                UserName = _userProvider.GetUsername(),
                Date = DateTime.Now.ToString(),
                Message = _userProvider.GetUsername() + " has left the club."
            };   
            
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, clubName);
            await Clients.Group(clubName).SendAsync("LeaveClub", chatMessage);
         //   await Clients.All.SendAsync("LeaveClub", Context.ConnectionId);
        }
        
        /*public async Task NewMessage(Message msg)
        {
            await Clients.All.SendAsync("MessageReceived", msg);
        }


        public Task SendMessageToAll(string message)
        {
            return Clients.All.SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToCaller(string message)
        {
            return Clients.Caller.SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToUser(string connectionId, string message)
        {
            return Clients.Client(connectionId).SendAsync("ReceiveMessage", message);
        }

        public Task SendMessageToGroup(string group, string message)
        {
            return Clients.Group(group).SendAsync("ReceiveMessage", message);
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.All.SendAsync("UserConnected", Context.ConnectionId);
            await base.OnConnectedAsync();
        }*/

        // public override async Task OnDisconnectedAsync(Exception ex)
        // {
        //     await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);
        //     await base.OnDisconnectedAsync(ex);
        // }
    }
}
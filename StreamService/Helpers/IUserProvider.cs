namespace UsersService.Helpers
{
    public interface IUserProvider
    {
        string GetUsername();
    }
}
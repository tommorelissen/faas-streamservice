using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace UsersService.Helpers
{
    public class UserProvider : IUserProvider
    {
        private readonly IHttpContextAccessor _context;

        public UserProvider (IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public string GetUsername()
        {
            return _context.HttpContext.User.Claims
                .First(i => i.Type == ClaimTypes.Name).Value;
        }
    }
}
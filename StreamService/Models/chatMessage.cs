namespace StreamService.Models
{
    public class ChatMessage
    {
        public string Id { get; set; }
        public string UserName { get; set; }    
        public string ClubName { get; set; }
        public string Message { get; set; }
        public string Date { get; set; }
    }
}